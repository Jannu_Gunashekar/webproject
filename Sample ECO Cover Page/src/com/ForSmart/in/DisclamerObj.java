package com.ForSmart.in;

public class DisclamerObj {
	
final static String DISCLAMER="Disclaimer: This email and any attachments are sent in strictest confidence for the sole use of the addressee and may contain legally privileged, confidential, and proprietary data. If you are not the intended recipient, please advise the sender by replying promptly to this email and then delete and destroy this email and any attachments without any further use, copying or forwarding.";

public String disclamerObj(){
	
	return DISCLAMER;
}
}
