package com.ForSmart.in;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Rectangle;

public class Doccumnetation {
	private static String FILE = "D:\\Forsmart files\\pdf SampleProg\\sampleECO.pdf";
	private static String LOGO = "D:\\Reslarge.png";
	
	public void doccument() throws Exception{
		
		InstructionMapObj insMapObj=new InstructionMapObj();
		MapObject mapObject=new MapObject();
		PlanMapObj planMapObject=new PlanMapObj();
		DisclamerObj disclamer=new DisclamerObj();
		
		Map mainMap=mapObject.mapObjects();
		Map insMap=insMapObj.insMapObj();
		Map planMap=planMapObject.planMapObj();
		String note=disclamer.disclamerObj();
		
		Document document = new Document();
		try{
			PdfWriter.getInstance(document, new FileOutputStream(FILE));
			document.open();
			
	        addMainContent(mainMap, document);
	        addInstructionContent(insMap, document);
	        addPlanContent(planMap, document);
	        
	        Paragraph space1=new Paragraph(" ");
	        Paragraph space2=new Paragraph(" ");
	        
	        document.add(space1);
	        document.add(space2);
	        
	        Paragraph praNote=new Paragraph(note);
	        document.add(praNote);// Paragraph content.
	        document.close();
	        System.out.println("Doccument is successfully created.");
	      //sending the Created PDF file document to the Gmail account.
			PDFMailing gmail=new PDFMailing();
			gmail.pdfMail(FILE);
	        
		}
		
		catch(Exception e){
			System.err.println(e);
		}
		
		
	}
private static PdfPCell getCell(String time, String date, Image logo, int alignment) {
		
		if(logo==null) {
			PdfPCell cell = new PdfPCell(new Phrase("Date:"+date+System.getProperty("line.separator")+System.getProperty("line.separator")+"Time:"+time)); 
		    cell.setPadding(0);
		    cell.setHorizontalAlignment(alignment);
		    cell.setBorder(PdfPCell.NO_BORDER);
		    
		    return cell;
		}
		else {
			PdfPCell cell = new PdfPCell(logo);
		    cell.setPadding(0);
		    cell.setHorizontalAlignment(alignment);
		    cell.setBorder(PdfPCell.NO_BORDER);
		    return cell;
		}
	}

private static void addEmptyLine(Paragraph paragraph, int number) {
    for (int i = 0; i < number; i++) {
        paragraph.add(new Paragraph(" "));
    }
}
private static void addMetaData(Document document) throws DocumentException {
	
	document.addTitle("Example PDF");
    document.addSubject("Attribute");
    document.addKeywords("Resideo,Item,Change order");
    document.addAuthor("XX.inc");
    document.addCreator("Creator");
    Rectangle rect= new Rectangle(577,825,18,15);
    rect.enableBorderSide(1);
    rect.enableBorderSide(2);
    rect.enableBorderSide(4);
    rect.enableBorderSide(8);
    rect.setBorderColor(BaseColor.BLACK);
    rect.setBorderWidth(1);
    document.add(rect);

}
private static void addMainContent(Map details, Document document) throws BadElementException, MalformedURLException, IOException{
	
	try{
		addMetaData(document);
		Image logo = Image.getInstance(LOGO);
	    logo.setAbsolutePosition(50f,50f);
	    logo.scaleAbsolute(1500f, 0f);
	    logo.scalePercent(0.3f*100);
	    
		Date date = new Date();  
    	SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		String strDate = formatter.format(date);
		formatter = new SimpleDateFormat("hh:mm:ss");
		String strTime= formatter.format(date);
    	
		PdfPTable headingTable = new PdfPTable(2);
		headingTable.setWidthPercentage(100);
		headingTable.addCell(getCell(null,null, logo,PdfPCell.ALIGN_LEFT));
		headingTable.addCell(getCell(strTime,strDate,null, PdfPCell.ALIGN_RIGHT));
    	document.add(headingTable);
    	Paragraph title = new Paragraph();
    	addEmptyLine(title, 1);
        
    	title.add(new Paragraph((String) details.get("type1"),new Font(Font.FontFamily.TIMES_ROMAN,18,Font.BOLD)));
    	title.setAlignment(Element.ALIGN_CENTER);
    	document.add(title);
    	
    
    	PdfPTable detailsTable = new PdfPTable(2);
    	detailsTable.setWidthPercentage(70f);;
    	document.add(new Paragraph(" "));
    	getMainTableWithInfo(detailsTable, details);
    	document.add(detailsTable);
		
	}
	catch(Exception e){
		System.err.println(e);
	}
}

private static void addInstructionContent(Map details, Document document) throws BadElementException, MalformedURLException, IOException{
	try{
		addMetaData(document);
		
		Paragraph title = new Paragraph();
    	addEmptyLine(title, 1);
    	
        
    	title.add(new Paragraph((String) details.get("type2"),new Font(Font.FontFamily.TIMES_ROMAN,18,Font.BOLD)));
    	title.setAlignment(Element.ALIGN_CENTER);
    	document.add(title);
    	
    
    	PdfPTable detailsTable = new PdfPTable(2);
    	detailsTable.setWidthPercentage(70f);;
    	document.add(new Paragraph(" "));
    	getInsTableWithInfo(detailsTable, details);
    	document.add(detailsTable);
    	
	}
	catch(Exception e){
		System.err.println(e);
	}
}
private static void addPlanContent(Map details, Document document) throws BadElementException, MalformedURLException, IOException{
	try{
		

		
		Paragraph title = new Paragraph();
    	addEmptyLine(title, 1);
        
    	title.add(new Paragraph((String) details.get("type3"),new Font(Font.FontFamily.TIMES_ROMAN,18,Font.BOLD)));
    	title.setAlignment(Element.ALIGN_CENTER);
    	document.add(title);
    	
    
    	PdfPTable detailsTable = new PdfPTable(2);
    	detailsTable.setWidthPercentage(70f);;
    	document.add(new Paragraph(" "));
    	addMetaData(document);
    	getPlanTableWithInfo(detailsTable, details);
    	document.add(detailsTable);
    	
	}
	catch(Exception e){
		System.err.println(e);
	}
}
private static void getMainTableWithInfo(PdfPTable detailsTable, Map details) throws DocumentException {
	PdfPCell heading = new PdfPCell(new Paragraph("Attributes"));
	heading.setHorizontalAlignment(Element.ALIGN_CENTER);
	detailsTable.addCell(heading);
	heading = new PdfPCell(new Paragraph("Values"));
	heading.setHorizontalAlignment(Element.ALIGN_CENTER);
    detailsTable.addCell(heading);
    String attributearr[]= {"Change  Number","Status","Change Type","Business Line(s)",
    		"Description","Reason For Change","Change Analyst","Workflow","Originator","Date Orginated","Date Released"};
    for (int i=0; i<=10;i++) {	
    		detailsTable.addCell(attributearr[i]);
    		String tempVal=(String) details.get(attributearr[i]);
    		
    		detailsTable.addCell(tempVal);
    }		
}
private static void getInsTableWithInfo(PdfPTable detailsTable, Map details) throws DocumentException {
	PdfPCell heading = new PdfPCell(new Paragraph("Attributes"));
	heading.setHorizontalAlignment(Element.ALIGN_CENTER);
	detailsTable.addCell(heading);
	heading = new PdfPCell(new Paragraph("Values"));
	heading.setHorizontalAlignment(Element.ALIGN_CENTER);
    detailsTable.addCell(heading);
    String attributearr[]= {"Company","Supply To","File Security","Design Center (Authority)",
    		"Accolade Project Resideo","Product Line(s)","Doc Control(MRP) to Notify","Where Built","Supplier Access","Effectivity Code","Use up/Upon Receipt PN ","Required Effective Date","Requires Regulatory Review?","Regulatory Approval Status","Regulatory Comments","Special Instructions/Comments","Created By","Date Entered State"};
    for (int i=0; i<=17;i++) {	
    		detailsTable.addCell(attributearr[i]);
    		String tempVal=(String) details.get(attributearr[i]);
    		
    		detailsTable.addCell(tempVal);
    }		
}
private static void getPlanTableWithInfo(PdfPTable detailsTable, Map details) throws DocumentException {
	PdfPCell heading = new PdfPCell(new Paragraph("Attributes"));
	heading.setHorizontalAlignment(Element.ALIGN_CENTER);
	detailsTable.addCell(heading);
	heading = new PdfPCell(new Paragraph("Values"));
	heading.setHorizontalAlignment(Element.ALIGN_CENTER);
    detailsTable.addCell(heading);
    String attributearr[]= {"Estimated Implementation Date","Implementation Remarks","Total Scrap Vlaue(USD)"};
    for (int i=0; i<=2;i++) {	
    		detailsTable.addCell(attributearr[i]);
    		String tempVal=(String) details.get(attributearr[i]);
    		
    		detailsTable.addCell(tempVal);
    }		
}
}
