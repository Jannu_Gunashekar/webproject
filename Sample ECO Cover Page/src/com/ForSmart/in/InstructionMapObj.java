package com.ForSmart.in;

import java.util.HashMap;
import java.util.Map;

public class InstructionMapObj {
	
	final static String TYPE="Implementation Instructions";
	final static String COMPANY=" ";
	final static String SUPPLY_TO=" ";
	final static String FILE_SECURITY="Yes";
	final static String DESIGN_CENTER="NY80 Melville";
	final static String ACCOLADE_PROJECT_RESIDEO="X_Administrative ISO/Process doccumentation, not related to product";
	final static String PRODUCT_LINE="X_Not Product Specific";
	final static String DOC_CONTROL="X_not applicable";
	final static String WHERE_BUILT=" ";
	final static String SUPPLIER_ACCESS=" ";
	final static String EFFECTIVE_CODE=" ";
	final static String USE_UP=" ";
	final static String REQUIRED_EFFECTIVE_DATE=" ";
	final static String REQUIRES_REGULATORY_REVIEW=" ";
	final static String REGULATORY_APPROVAL_STATUS=" ";
	final static String REGULATORY_COMMENTS=" ";
	final static String SPECIAL_INS=" ";
	final static String CREATED_BY="McKinley,Jennifer(201952)";
	final static String DATE_ENTERED_STATE="02/25/2020, 06:59:04 PM IST";
	
	public Map insMapObj(){
		Map insMap=new HashMap<String, String>();
		
	insMap.put("type2",TYPE );
	insMap.put("Company", COMPANY);
	insMap.put("Supply To", SUPPLY_TO);
	insMap.put("File Security", FILE_SECURITY);
	insMap.put("Design Center (Authority)",DESIGN_CENTER);
	insMap.put("Accolade Project Resideo", ACCOLADE_PROJECT_RESIDEO);
	insMap.put("Product Line(s)", PRODUCT_LINE);
	insMap.put("Doc Control(MRP) to Notify", DOC_CONTROL);
	insMap.put("Where Built", WHERE_BUILT);
	insMap.put("Supplier Access", SUPPLIER_ACCESS);
	insMap.put("Effectivity Code", EFFECTIVE_CODE);
	insMap.put("Use up/Upon Receipt PN ",USE_UP);
	insMap.put("Required Effective Date", REQUIRED_EFFECTIVE_DATE);
	insMap.put("Requires Regulatory Review?",REQUIRES_REGULATORY_REVIEW);
	insMap.put("Regulatory Approval Status", REGULATORY_APPROVAL_STATUS);
	insMap.put("Regulatory Comments", REGULATORY_COMMENTS);
	insMap.put("Special Instructions/Comments", SPECIAL_INS);
	insMap.put("Created By", CREATED_BY);
	insMap.put("Date Entered State",DATE_ENTERED_STATE);
	
		return insMap;
	}

}
