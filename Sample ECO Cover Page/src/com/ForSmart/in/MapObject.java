package com.ForSmart.in;

import java.util.HashMap;
import java.util.Map;

public class MapObject {
	final static String TYPE= "Implementation Instructions | Implementation Plan";
	final static String CHANGE_ORDER_NUMBER= "REC-64780";
	final static String CHANGE_ORDER_STATUS= "Completed";
	final static String CHANGE_ORDER_TYPE= "ECO";
	final static String BUSINESS_LINES= "X-Not Business Specific";
	final static String CHANGE_ORDER_DESC= "Updated Agile Uniques SDK-new library files";
	final static String REASON_FOR_CHANGE= "Need Updated library files pew minor update RUP9";
	final static String CHANGE_ANALYST= "Melville Change Analysts";
	final static String WORKFLOW= "Purchased Item change orders";
	final static String ORIGINATOR= "McKinley,Jennifer(201952)";
	final static String DATE_ORIGINATED= "02/24/2020,11:24:03 PM IST";
	final static String DATE_REALEASED= "02/25/2020,06:59:03 PM IST";
	final static String FINAL_COMPLETE_DATE="02/25/202,06:59:04 PM IST";
     public Map mapObjects(){
    	 Map map=new HashMap<String, String>();
    	 map.put("type1", TYPE);
    	 map.put("Change  Number", CHANGE_ORDER_NUMBER);
    	 map.put("Status", CHANGE_ORDER_STATUS);
    	 map.put("Change Type", CHANGE_ORDER_TYPE);
    	 map.put("Business Line(s)", BUSINESS_LINES);
    	 map.put("Description", CHANGE_ORDER_DESC);
    	 map.put("Reason For Change", REASON_FOR_CHANGE);
    	 map.put("Change Analyst", CHANGE_ANALYST);		
    	 map.put("Workflow", WORKFLOW);
    	 map.put("Originator", ORIGINATOR);
    	 map.put("Date Orginated", DATE_ORIGINATED);
    	 map.put("Date Released", DATE_ORIGINATED);  	 
    	return map; 
     }

}
