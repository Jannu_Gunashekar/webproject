package com.ForSmart.in;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class PDFMailing {

	public void pdfMail(String file) throws Exception{
		//Creating the Properties object.
		Properties prop=new Properties();
		
		//Creating the object, to retrieve the data from the Properties file.
		InputStream is=new FileInputStream("dataConfig.properties");
		
		//pulling the data from file to Properties object.
		prop.load(is);


//Sender email-ID and Password.
		final String senderEmail=prop.getProperty("userID");
		final String password=prop.getProperty("password");
				
				//setting the Properties.
				Properties props=new Properties();
				props.put("mail.smtp.host", "smtp.gmail.com"); //SMTP Host
				props.put("mail.smtp.socketFactory.port", "465"); //SSL Port
				props.put("mail.smtp.socketFactory.class",
						"javax.net.ssl.SSLSocketFactory"); //SSL Factory Class
				props.put("mail.smtp.auth", "true"); //Enabling SMTP Authentication
				props.put("mail.smtp.port", "465"); //SMTP Port
				
				//Authenticating the mailID of the sender.
					Authenticator auth = new Authenticator() {
						//override the getPasswordAuthentication method
						protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
							return new javax.mail.PasswordAuthentication(senderEmail, password);
						}
					};
				 
					//Creating and getting the Session Object.
				 Session session=Session.getInstance(props, auth);
				 
				 //Setting the From, To, Subject, MessageBody. 
				 try{
					 Message message=new MimeMessage(session);
					 message.setFrom(new InternetAddress("monkeykingluffy65@gmail.com"));
					 message.setRecipients(Message.RecipientType.TO,InternetAddress.parse("gunashekar.jannu96@gmail.com"));
					 message.setSubject("Sample ECO PDF file");
					 message.setText("This a Sample ECO PDF file.");
					 
					 MimeBodyPart bodyPart=new MimeBodyPart();
					  Multipart multipart=new MimeMultipart();
					  
					  bodyPart.setText("This is multipart Text.");
					  
					  
					  //Attachments for any file.
					MimeBodyPart pdfAttachment=new MimeBodyPart();
					pdfAttachment.attachFile(file);
					
					//Attach the Body part to the Multipart.
					multipart.addBodyPart(bodyPart);
					multipart.addBodyPart(pdfAttachment);
					
					//Associate multipart to the message.
					message.setContent(multipart);
					  
					  System.out.println("Sending mail is in process.......");
					 
					  //sending the message to-address mail.
					 Transport.send(message);
					 System.out.println("Mail has been sent sucessfully.");
				 }
				 catch(Exception e){
					 throw new RuntimeException(e);
					 
				 }
	}
}
