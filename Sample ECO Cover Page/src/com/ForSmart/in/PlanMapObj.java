package com.ForSmart.in;

import java.util.HashMap;
import java.util.Map;

public class PlanMapObj {
	
	final static String TYPE="Implementation Plan";
	final static String ESTIMATED_IMPLEMENTATION_DATE=" ";
	final static String IMPLEMENTATION_REMARKS=" ";
	final static String TOTAL_SCRAP_VALUE=" ";
	
	public Map planMapObj(){
		Map planMap=new HashMap<String, String>();
		
		planMap.put("type3", TYPE);
		planMap.put("Estimated Implementation Date", ESTIMATED_IMPLEMENTATION_DATE);
		planMap.put("Implementation Remarks",IMPLEMENTATION_REMARKS);
		planMap.put("Total Scrap Vlaue(USD)",TOTAL_SCRAP_VALUE);
		
		return planMap;
	}
	

}
